// Import some code we need
import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

// Create a react component
export default class DayItem extends Component {

  render() {
    return (
      <Text style={this.dynStyle()}>
      {this.props.day}
      </Text>
    );
  }

  dynStyle() {
    return(
      this.color()
    );
  }

  color() {
    var daysUntil;
    var opacity = 1 / daysUntil;
    return 'rgba(0,0,0,'+opacity+')';
  }

}



// Style the React component
var styless = StyleSheet.create({
  day: {
    fontSize: 18,
    color: '#0000FF'
  }
});

// Make this code available elsewhere
module.exports = DayItem;
