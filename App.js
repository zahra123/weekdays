// Import some code we need
import Moment from 'moment';
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import DayItem from './src/day-item';

var DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

// Create a react component
// Test 123123123132
export default class Weekdays extends Component {

  render() {
    return (
      <View style={styles.container}>
      {this.days()}
      </View>
    );
  }

  days() {
    var daysItems = [];
    for (var i = 0; i < 7; i++) {
      var day = Moment().add(i, 'days').format('dddd');
      daysItems.push(
        <DayItem key={day} day={day} daysUntil={i} />
      )
    }
    return(daysItems);
  }

}


// Style the React component
var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
